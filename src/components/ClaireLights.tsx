import React, {useEffect, useState} from 'react';
import {CirclePicker, ColorResult} from "react-color";
import {IonCard, IonSelect, IonLabel, IonSelectOption, IonCardContent, IonCardTitle, IonButton, IonRange, IonIcon} from "@ionic/react";
import Thingamiverse from '../Thingimaverse'


const ClairLights:React.FC = () => {


  const [fill, setFill] = useState<string>('stripe');
  const [effect, setEffect] = useState<string>('solid');
  const [color, setColor] = useState<string>('#00FF00');
  const [colors, setColors] = useState<string[]>([]);
  const [speed, setSpeed] = useState<number>( 50 );
  const [brightness, setBrightness] = useState<number>();

  useEffect(lights);

  function lights() {
    if (colors && colors.length) {

      let interval = parseFloat((0.25 - (speed/400.0)).toFixed(2));
      let trimColor = colors && colors.length ? (colors.join('')).replace(/#/g, '') : "";
      let obj:any = {stripe: trimColor, fill:fill, effect: effect, interval: interval};
      Thingamiverse.send("dev", "lights",  obj);
    }
  }

  function turnOff() {
    Thingamiverse.send("dev", "off");
  }

  function colorsAsStripe() {
    let stripe:String = "";
    if (colors && colors.length) {
      stripe = colors.join();
    }
    return stripe;
  }

  function resetColors() {
    let newColors:string[] = [];
    setColors(newColors);
  }

  function addColor(color:string) {
    let newColors:string[]  = colors.splice(0);
    newColors.push(color);
    setColors(newColors);
  }

  function handleColorChange(color:ColorResult) {
    addColor(color.hex);
  }

  return (
    <IonCard className="ClaireLights">
      <IonCardTitle>
        Claire's Lights
      </IonCardTitle>
      <IonCardContent>
        <IonLabel>Fill</IonLabel>
        <IonSelect placeholder="Fill" value={fill} onIonChange={e => setFill(e.detail.value) } interface="popover">
          <IonSelectOption value="spread">Spread</IonSelectOption>
          <IonSelectOption value="stripe">Stripe</IonSelectOption>
        </IonSelect>
        <IonLabel>Effect</IonLabel>
        <IonSelect placeholder="Effect" value={effect} onIonChange={e => setEffect(e.detail.value)} interface="popover" >
          <IonSelectOption value="solid">Solid</IonSelectOption>
          <IonSelectOption value="down">Chase</IonSelectOption>
        </IonSelect>
        <IonLabel>Speed</IonLabel>
        <IonRange debounce={500} value={speed} min={1} max={100} onIonChange={(e) => setSpeed(e.detail.value as number)}>\
          <IonIcon slot="start" name="speed"/>
          <IonIcon slot="end" name="speed"/>
        </IonRange>
        <p>Pick a color and watch it change</p>
        <CirclePicker onChangeComplete={handleColorChange}></CirclePicker>
        <IonButton onClick={e => turnOff()}>Off</IonButton>
        <IonButton onClick={e => resetColors()}>Reset</IonButton>

      </IonCardContent>
    </IonCard>
  );
}

export default ClairLights;