import axios from 'axios';

class ThingamiverseClass {

  devices:Array<Object>  = [];
  serviceUrl:String = ""

  constructor() {
    if (window.location.host.search('localhost') >= 0) {
      // Assume ports that start iwth 818 (e.g. 8180-8189 will connect to demo environment)
      // so that devs with no lando work.
      this.serviceUrl = 'http://192.168.1.100/service/';
    } else {
      this.serviceUrl = 'http://192.168.1.100/service/';
    }
  }

  send(name:string, action:string, obj:any = {}) {
    let message:any = {};
    message.topic = name + '/' + action;
    message.data = obj;
    let data:string = JSON.stringify(message);
    axios.post(this.serviceUrl + 'send', data);
  }

}

let Thingamiverse:ThingamiverseClass = new ThingamiverseClass();

export default Thingamiverse;