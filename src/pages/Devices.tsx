import React from 'react';
import {IonContent, IonHeader, IonPage, IonTitle, IonToolbar} from '@ionic/react';
import './Devices.css';
import ClaireLights from '../components/ClaireLights';

const Devices: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Devices</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <ClaireLights></ClaireLights>
      </IonContent>
    </IonPage>
  );
};

export default Devices;
